package happyspace.android;

import happyspace.persistence.User;
import happyspace.web.LoginData;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginLayout extends Activity {
	EditText un, pw;
	TextView error, debug;
	Button ok, create_account;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		// some extra parameters to be sent to the next activity
		final String EXTRA_LOGIN = null;

		un = (EditText) findViewById(R.id.et_un);
		pw = (EditText) findViewById(R.id.et_pw);
		ok = (Button) findViewById(R.id.btn_login);
		create_account = (Button) findViewById(R.id.bt_account);
		error = (TextView) findViewById(R.id.tv_error);
		debug = (TextView) findViewById(R.id.tv_debug);

		ok.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String url = "http://10.0.2.2:8080"; // Development Machine
//				 String url="http://happyspace.no-ip.org:8080"; // Live Server
				try {
					LoginData loginData = new LoginData();
					loginData.setUserid(un.getText().toString());
					loginData.setPassword(pw.getText().toString());
					RestClient restClient = new RestClient(url);
					User user = restClient.login(loginData);

					if (user != null) {
						error.setText("Login is successful! " + user.getName());

						Intent intent = new Intent(LoginLayout.this, Login_display.class);
						intent.putExtra(EXTRA_LOGIN, un.getText().toString());
						startActivity(intent);
					} else
						error.setText("Sorry!! Incorrect Username or Password");

					System.out.println("" + error.toString());
				} catch (Exception e) {
					un.setText(e.toString());
				}
			}

		});
		create_account.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(LoginLayout.this, Register.class);
				startActivity(intent);
			}

		});
	}
}
