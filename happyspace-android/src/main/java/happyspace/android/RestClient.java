package happyspace.android;

import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import happyspace.persistence.User;
import happyspace.web.LoginData;

public class RestClient {
	private RestTemplate restTemplate;

	private static final String loginResource = "/ws/login";
	private static final String signUpResource = "/ws/signup";
	
	private String baseUri;
	
	
	public RestClient(String baseUri) {
		this.baseUri = baseUri;
		restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
	}

	public User login(LoginData loginData) {
		String invokeUrl = baseUri + loginResource;
		User user = restTemplate.postForObject(invokeUrl, loginData, User.class);
		return user;
	}
	
	public User signUp(User newUser) {
		String invokeUrl = baseUri + signUpResource;
		User user = restTemplate.postForObject(invokeUrl, newUser, User.class);
		return user;
	}
}
