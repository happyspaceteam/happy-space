package happyspace.android;


import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;


import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ImageView;

public class Login_display extends Activity {

	// to get the extra parameter from the first activity
	final String EXTRA_LOGIN = null;

	Button getqr;
	ImageView image;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_display);

		Intent intent = getIntent();
		TextView login = (TextView) findViewById(R.id.login_display);
		if (intent != null) {
			login.setText(intent.getStringExtra(EXTRA_LOGIN));
		}
		getqr = (Button) findViewById(R.id.bt_getqr);
		image = (ImageView) findViewById(R.id.image_display);
		getqr.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String plop;
				URL url = null;
				try {
					url = new URL("http://10.0.2.2:8080/ws/qr/" + getIntent().getStringExtra(EXTRA_LOGIN));
//					url = new URL("http://happyspace.no-ip.org:8080/ws/qr/" + getIntent().getStringExtra(EXTRA_LOGIN));
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// TODO Auto-generated method stub
				// image.setImageURI(uri);
				try {
					image.setImageBitmap(BitmapFactory.decodeStream(url
							.openConnection().getInputStream()));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		});
	}

}