package happyspace.android;

import happyspace.persistence.User;
import happyspace.web.LoginData;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Register extends Activity {
	EditText username,name,email,password,phone;
	Button create;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        
        username=(EditText)findViewById(R.id.create_username);
        name=(EditText)findViewById(R.id.create_name);
        email=(EditText)findViewById(R.id.create_email);
        phone=(EditText)findViewById(R.id.create_phone);
        password=(EditText)findViewById(R.id.create_password);
        
        
        create=(Button)findViewById(R.id.bt_create_account);
        create.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v) {
				String url = "http://10.0.2.2:8080"; // Development Machine
//				 String url="http://happyspace.no-ip.org:8080"; // Live Server
            	try {
					User newUser = new User();
					newUser.setUsername(username.getText().toString());
					newUser.setName(name.getText().toString());
					newUser.setEmail(email.getText().toString());
					newUser.setPhoneNumber(phone.getText().toString());
					newUser.setPassword(password.getText().toString());
					RestClient restClient = new RestClient(url);
					User user = restClient.signUp(newUser);

            	    if(user != null){
            			Intent intent = new Intent(Register.this, LoginLayout.class);
            			startActivity(intent);
            		} else {
//            	    	error.setText("Sorry!! Incorrect Username or Password");
//            	    
//            	    	System.out.println("" + error.toString());
            		}
            	} catch (Exception e) {
            		
            	}
			}
        	
        });
        
    } 
}