/*
 * jQuery.ScrollTo
 * Copyright (c) 2008 Ariel Flesler - aflesler(at)gmail(dot)com | http://flesler.blogspot.com
 */
;(function( $ ){

	var $scrollTo = $.scrollTo = function( target, duration, settings ){
		$scrollTo.window().scrollTo( target, duration, settings );
	};

	$scrollTo.defaults = {
		axis:'y',
		duration:1
	};

	$scrollTo.window = function(){
		return $( $.browser.safari ? 'body' : 'html' );
	};

	$.fn.scrollTo = function( target, duration, settings ){
		if( typeof duration == 'object' ){
			settings = duration;
			duration = 0;
		}
		settings = $.extend( {}, $scrollTo.defaults, settings );
		duration = duration || settings.speed || settings.duration;
		settings.queue = settings.queue && settings.axis.length > 1;
		if( settings.queue )
			duration /= 2;//let's keep the overall speed, the same.
		settings.offset = both( settings.offset );
		settings.over = both( settings.over );

		return this.each(function(){
			var elem = this, $elem = $(elem),
				t = target, toff, attr = {},
				win = $elem.is('html,body');
			switch( typeof t ){
				case 'number'://will pass the regex
				case 'string':
					if( /^([+-]=)?\d+(px)?$/.test(t) ){
						t = both( t );
						break;//we are done
					}
					t = $(t,this);// relative selector, no break!
				case 'object':
					if( t.is || t.style )//DOM/jQuery
						toff = (t = $(t)).offset();//get the real position of the target 
			}
			$.each( settings.axis.split(''), function( i, axis ){
				var Pos	= axis == 'x' ? 'Left' : 'Top',
					pos = Pos.toLowerCase(),
					key = 'scroll' + Pos,
					act = elem[key],
					Dim = axis == 'x' ? 'Width' : 'Height',
					dim = Dim.toLowerCase();

				if( toff ){//jQuery/DOM
					attr[key] = toff[pos] + ( win ? 0 : act - $elem.offset()[pos] );

					if( settings.margin ){//if it's a dom element, reduce the margin
						attr[key] -= parseInt(t.css('margin'+Pos)) || 0;
						attr[key] -= parseInt(t.css('border'+Pos+'Width')) || 0;
					}
					
					attr[key] += settings.offset[pos] || 0;
					
					if( settings.over[pos] )
						attr[key] += t[dim]() * settings.over[pos];
				}else
					attr[key] = t[pos];

				if( /^\d+$/.test(attr[key]) )
					attr[key] = attr[key] <= 0 ? 0 : Math.min( attr[key], max(Dim) );//check the limits

				if( !i && settings.queue ){		
					if( act != attr[key] )
						animate( settings.onAfterFirst );
					delete attr[key];
				}
			});			
			animate( settings.onAfter );			

			function animate( callback ){
				$elem.animate( attr, duration, settings.easing, callback && function(){
					callback.call(this, target);
				});
			};
			function max( Dim ){
				var el = win ? $.browser.opera ? document.body : document.documentElement : elem;
				return el['scroll'+Dim] - el['client'+Dim];
			};
		});
	};

	function both( val ){
		return typeof val == 'object' ? val : { top:val, left:val };
	};

})( jQuery );

/**
 * jQuery.LocalScroll
 * Copyright (c) 2007-2008 Ariel Flesler - aflesler(at)gmail(dot)com | http://flesler.blogspot.com
 **/
;(function( $ ){
	var URI = location.href.replace(/#.*/,'');

	var $localScroll = $.localScroll = function( settings ){
		$('body').localScroll( settings );
	};

	$localScroll.defaults = {
		duration:1000,
		axis:'y',
		event:'click',
		stop:true
	};

	$localScroll.hash = function( settings ){
		settings = $.extend( {}, $localScroll.defaults, settings );
		settings.hash = false;
		if( location.hash )
			setTimeout(function(){ scroll( 0, location, settings ); }, 0 );
	};

	$.fn.localScroll = function( settings ){
		settings = $.extend( {}, $localScroll.defaults, settings );

		return ( settings.persistent || settings.lazy ) 
				? this.bind( settings.event, function( e ){
					var a = $([e.target, e.target.parentNode]).filter(filter)[0];
					a && scroll( e, a, settings );
				})
				: this.find('a')
						.filter( filter ).bind( settings.event, function(e){
							scroll( e, this, settings );
						}).end()
					.end();

		function filter() {
			return !!this.href && !!this.hash && this.href.replace(this.hash,'') == URI && (!settings.filter || $(this).is( settings.filter ));
		};
	};

	function scroll( e, link, settings ){
		var id = link.hash.slice(1),
			elem = document.getElementById(id) || document.getElementsByName(id)[0];
		if ( elem ){
			e && e.preventDefault();
			var $target = $( settings.target || $.scrollTo.window() );

			if( settings.lock && $target.is(':animated') ||
			settings.onBefore && settings.onBefore.call(link, e, elem, $target) === false ) return;

			if( settings.stop )
				$target.queue('fx',[]).stop();
			$target
				.scrollTo( elem, settings )
				.trigger('notify.serialScroll',[elem]);
			if( settings.hash )
				$target.queue(function(){
					location = link.hash;
				});
		}
	};

})( jQuery );

/**
 * jQuery.serialScroll
 * Copyright (c) 2007-2008 Ariel Flesler - aflesler(at)gmail(dot)com | http://flesler.blogspot.com
 */
;(function( $ ){

	var $serialScroll = $.serialScroll = function( settings ){
		$.scrollTo.window().serialScroll( settings );
	};

	$serialScroll.defaults = {
		duration:1000, 
		axis:'y', 
		event:'click',
		start:0, 
		step:1, 
		lock:true,
		cycle:true, 
		constant:true
	};

	$.fn.serialScroll = function( settings ){
		settings = $.extend( {}, $serialScroll.defaults, settings );
		var event = settings.event, 
			step = settings.step, 
			lazy = settings.lazy;

		return this.each(function(){
			var 
				context = settings.target ? this : document, 
				$pane = $(settings.target || this, context),
				pane = $pane[0],
				items = settings.items, 
				active = settings.start,
				auto = settings.interval,
				nav = settings.navigation,
				timer; 

			if( !lazy )
				items = getItems();

			if( settings.force )
				jump( {}, active );

			$(settings.prev||[], context).bind( event, -step, move );
			$(settings.next||[], context).bind( event, step, move );

			if( !pane.ssbound )
				$pane
					.bind('prev.serialScroll', -step, move ) 
					.bind('next.serialScroll', step, move )
					.bind('goto.serialScroll', jump );
			if( auto )
				$pane
					.bind('start.serialScroll', function(e){
						if( !auto ){
							clear();
							auto = true;
							next();
						}
					 })
					.bind('stop.serialScroll', function(){
						clear();
						auto = false;
					});
			$pane.bind('notify.serialScroll', function(e, elem){
				var i = index(elem);
				if( i > -1 )
					active = i;
			});
			pane.ssbound = true;

			if( settings.jump )
				(lazy ? $pane : getItems()).bind( event, function( e ){
					jump( e, index(e.target) );
				});

			if( nav )
				nav = $(nav, context).bind(event, function( e ){
					e.data = Math.round(getItems().length / nav.length) * nav.index(this);
					jump( e, this );
				});

			function move( e ){
				e.data += active;
				jump( e, this );
			};
			function jump( e, button ){
				if( !isNaN(button) ){
					e.data = button;
					button = pane;
				}

				var
					pos = e.data, n,
					real = e.type, 
					$items = settings.exclude ? getItems().slice(0,-settings.exclude) : getItems(),
					limit = $items.length,
					elem = $items[pos],
					duration = settings.duration;

				if( real )
					e.preventDefault();

				if( auto ){
					clear();
					timer = setTimeout( next, settings.interval ); 
				}

				if( !elem ){ 
					n = pos < 0 ? 0 : limit - 1;
					if( active != n )
						pos = n;
					else if( !settings.cycle )
						return;
					else
						pos = limit - n - 1;
					elem = $items[pos];
				}

				if( !elem || real && active == pos || 
					settings.lock && $pane.is(':animated') ||
					real && settings.onBefore && 
					settings.onBefore.call(button, e, elem, $pane, getItems(), pos) === false ) return;

				if( settings.stop )
					$pane.queue('fx',[]).stop();

				if( settings.constant )
					duration = Math.abs(duration/step * (active - pos ));

				$pane
					.scrollTo( elem, duration, settings )
					.trigger('notify.serialScroll',[pos]);
			};
			function next(){
				$pane.trigger('next.serialScroll');
			};
			function clear(){
				clearTimeout(timer);
			};
			function getItems(){
				return $( items, pane );
			};
			function index( elem ){
				if( !isNaN(elem) ) return elem;
				var $items = getItems(), i;
				while(( i = $items.index(elem)) == -1 && elem != pane )
					elem = elem.parentNode;
				return i;
			};
		});
	};

})( jQuery );

// when the DOM is ready...
$(document).ready(function () {

    var $panels = $('#main_right .scrollContainer > div');
    var $container = $('#main_right .scrollContainer');

    var horizontal = false;

    if (horizontal) {
        $panels.css({
            'float' : 'left',
            'position' : 'relative' 
        });

        $container.css('width', $panels[0].offsetWidth * $panels.length);
    }

    var $scroll = $('#main_right .scrollContainer').css('overflow', 'hidden');

    function selectNav() {
        $(this)
            .parents('ul:first')
                .find('a')
                    .removeClass('selected')
                .end()
            .end()
            .addClass('selected');
    }

    $('#menu .main_menu').find('a').click(selectNav);

    function trigger(data) {
        var el = $('#menu .main_menu').find('a[href$="' + data.id + '"]').get(0);
        selectNav.call(el);
    }

    if (window.location.hash) {
        trigger({ id : window.location.hash.substr(1) });
    } else {
        $('ul.main_menu a:first').click();
    }

    var offset = parseInt((horizontal ? 
        $container.css('paddingTop') : 
        $container.css('paddingLeft')) 
        || 0) * -1;


    var scrollOptions = {
        target: $scroll, 
		
        items: $panels,

        navigation: '.main_menu a',

        prev: 'img.left', 
        next: 'img.right',

        axis: 'xy',

        onAfter: trigger, 

        offset: offset,

        duration: 500,

        easing: 'swing'
    };

    $('#main_right').serialScroll(scrollOptions);

    $.localScroll(scrollOptions);
    scrollOptions.duration = 1;
    $.localScroll.hash(scrollOptions);

});

