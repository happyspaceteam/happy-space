package happyspace.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.config.ListFactoryBean;
import org.springframework.core.io.Resource;

public class ResourceListFactoryBean extends ListFactoryBean {

    private static final String COMMENT_INDICATOR = "#";

    public ResourceListFactoryBean() {
        super();
    }

    public void setResource(Resource resource) throws IOException {
        Scanner scanner = new Scanner(resource.getInputStream());
        try {
            List<String> source = new ArrayList<String>();
            while (scanner.hasNextLine()) {
                String nextLine = scanner.nextLine();
                if (!nextLine.startsWith(COMMENT_INDICATOR)) {
                    source.add(nextLine);
                }
            }
            super.setSourceList(source);
        }

        finally {
            scanner.close();
        }
    }
}
