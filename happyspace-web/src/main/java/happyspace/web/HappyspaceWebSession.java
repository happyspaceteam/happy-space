package happyspace.web;

import happyspace.persistence.User;

import org.apache.wicket.injection.Injector;
import org.apache.wicket.protocol.http.WebSession;
import org.apache.wicket.request.Request;
import org.apache.wicket.spring.injection.annot.SpringBean;


public class HappyspaceWebSession extends WebSession {
    private static final long      serialVersionUID = 957613545615370846L;
    private User                   user;

    @SpringBean
    private ILoginService loginService;

    public HappyspaceWebSession(Request request) {
        super(request);
        Injector.get().inject(this);
    }

    public static HappyspaceWebSession get() {
        return (HappyspaceWebSession) WebSession.get();
    }

    public boolean isUserAuthenticated() {
        return (user != null);
    }

    public void logout() {
        user = null;
    }

    public boolean login(LoginData loginData) {
        User authenticatedUser = loginService.login(loginData);
        user = authenticatedUser;
        return (user != null);
    }

    public User getUser() {
        return user;
    }

}
