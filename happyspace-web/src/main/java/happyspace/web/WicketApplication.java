package happyspace.web;

import org.apache.wicket.Page;
import org.apache.wicket.Session;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.Response;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wicketstuff.annotation.scan.AnnotatedMountScanner;

public class WicketApplication extends WebApplication {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private String homePageClassName;

	@Override
	public void init() {
		super.init();
		logger.info("Initializing Happyspace Wicket Application");
		getComponentInstantiationListeners().add(
				new SpringComponentInjector(this));
		logger.info("Spring Component Injector is created.");
		try {
			logger.info("Scanning packages for mounted wicket pages.");
			new AnnotatedMountScanner().scanPackage(
					Class.forName(homePageClassName).getPackage().getName())
					.mount(this);
		} catch (ClassNotFoundException e) {
			logger.error(
					"The provided home page class(" + homePageClassName
							+ ") could not be found on the classpath.", e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Class<? extends Page> getHomePage() {
		Class<?> clazz = null;
		try {
			clazz = Class.forName(homePageClassName);
		} catch (ClassNotFoundException e) {
			logger.error(
					"The provided home page class(" + homePageClassName
							+ ") could not be found on the classpath.", e);
		}
		return (Class<? extends Page>) clazz;
	}

	@Override
	public Session newSession(Request request, Response response) {
		logger.info("Creating a new session");
		Session session = new HappyspaceWebSession(request);
		logger.info("Returning Session");
		return session;
	}

	public void setHomePageClassName(String homePageClassName) {
		this.homePageClassName = homePageClassName;
	}
}
