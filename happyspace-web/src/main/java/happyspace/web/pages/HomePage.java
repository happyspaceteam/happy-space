package happyspace.web.pages;

import happyspace.persistence.User;
import happyspace.web.ILoginService;
import happyspace.web.LoginData;

import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.PropertyModel;

public class HomePage extends WebPage {
	private static final long serialVersionUID = 1L;
	LoginData loginData = new LoginData();
	User newUser = new User(); 
	
	@SpringBean
	private ILoginService loginService;
	
	public HomePage() {
		super();
	}

	public HomePage(final PageParameters parameters) {
		super(parameters);
		add(new Form("loginForm") {
			{
				add(new TextField<String>("userid", new PropertyModel<String>(loginData, "userid")));
				add(new PasswordTextField("password", new PropertyModel<String>(loginData, "password")));
				add(new TextField<String>("email", new PropertyModel<String>(newUser, "email")));
				add(new TextField<String>("name", new PropertyModel<String>(newUser, "name")));
				add(new TextField<String>("phone_number", new PropertyModel<String>(newUser, "phoneNumber")));
				
				add(new Button("loginBtn") {
					private static final long serialVersionUID = 1L;

					@Override
					public void onSubmit() {
						super.onSubmit();
						User user = loginService.login(loginData);
						if (user != null) {
							setResponsePage(new UserPage());
						} else {
							System.out.println("login unsuccessful");
						}
					}
				});
				
				add(new Button("sendButton") {
					private static final long serialVersionUID = 1L;

					@Override
					public void onSubmit() {
						super.onSubmit();
						newUser.setUsername(loginData.getUserid());
						newUser.setPassword(loginData.getPassword());
						User user = loginService.signUp(newUser);
						if (user != null) {
							setResponsePage(new UserPage());
						} else {
							System.out.println("sign up unsuccessful");
						}
					}
				});
			}
		});
	}
}
