package happyspace.web;

import java.awt.image.BufferedImage;

import happyspace.persistence.User;

public interface ILoginService {

	User login(LoginData loginData);
	User signUp(User newUser);
	BufferedImage getQrCode(String username);
	String openDoor(String text);

}
