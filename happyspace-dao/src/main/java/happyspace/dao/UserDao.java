package happyspace.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import happyspace.persistence.User;


public class UserDao extends AbstractHibernateDao<User> implements IUserDao {

	public UserDao() {
		super(User.class);
	}

	public User getUserByUsername(String username) {
        Criteria criteria = getSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("username", username));
        return ((User) criteria.uniqueResult());
	}

	public User getUserByNamePhoneNumberAndEmail(String name,
			String phoneNumber, String email) {
        Criteria criteria = getSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("name", name));
        criteria.add(Restrictions.eq("phoneNumber", phoneNumber));
        criteria.add(Restrictions.eq("email", email));
        return ((User) criteria.uniqueResult());
	}

}
