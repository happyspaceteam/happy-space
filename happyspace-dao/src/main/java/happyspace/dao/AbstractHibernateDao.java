package happyspace.dao;

import java.io.Serializable;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;


public abstract class AbstractHibernateDao<T> extends HibernateDaoSupport implements IHibernateDao<T> {
    private Class<T> clazz;

    public AbstractHibernateDao(Class<T> clazz) {
        this.clazz = clazz;
    }

    public T persist(T entity) {
        getSession().persist(entity);
        return entity;
    }

    @SuppressWarnings("unchecked")
    public T getById(Serializable id) {
        return (T) getSession().get(clazz, id);
    }

    public T update(T entity) {
        getSession().update(entity);
        return entity;
    }
    
    public T save(T entity) {
        getSession().save(entity);
        return entity;
    }

}
