package happyspace.dao;

import java.io.Serializable;


public interface IHibernateDao<T> {
	public T save(T entity);
	
    public T persist(T entity);

    public T getById(Serializable id);

    public T update(T entity);
}
