package happyspace.dao;

import happyspace.persistence.User;

public interface IUserDao extends IHibernateDao<User>{

	public User getUserByUsername(String username);

	public User getUserByNamePhoneNumberAndEmail(String name, String phoneNumber, String email);

}
