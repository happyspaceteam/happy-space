package happyspace.service;


import happyspace.web.ILoginService;

import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import com.github.sarxos.webcam.ds.buildin.WebcamDefaultDevice;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;


public class WebCamQRCodeExample extends JFrame implements Runnable, ThreadFactory {
	private static final long serialVersionUID = 6441489157408381878L;
	private Executor executor = Executors.newSingleThreadExecutor(this);
	private ILoginService loginService;

	private Webcam webcam = null;
	private WebcamPanel panel = null;
	private JTextArea textarea = null;
	private JScrollPane sbrText = null;

	public WebCamQRCodeExample() {
		super();

		setLayout(new FlowLayout());
		setTitle("Read QR / Bar Code With Webcam");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		webcam = Webcam.getWebcams().get(0);
		webcam.setViewSize(WebcamDefaultDevice.SIZE_QVGA);

		panel = new WebcamPanel(webcam);
		panel.setPreferredSize(WebcamDefaultDevice.SIZE_QVGA);

		textarea = new JTextArea();
		textarea.setEditable(false);
		textarea.setPreferredSize(WebcamDefaultDevice.SIZE_QVGA);
		sbrText = new JScrollPane(textarea);
		sbrText.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		add(panel);
		add(sbrText);

		pack();
		setVisible(true);

		executor.execute(this);
	}

	public void run() {
		try {
		do {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			Result result = null;
			BufferedImage image = null;

			if (webcam.isOpen()) {

				if ((image = webcam.getImage()) == null) {
					continue;
				}
				
				LuminanceSource source = new BufferedImageLuminanceSource(image);
				BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

				try {
					result = new MultiFormatReader().decode(bitmap);
				} catch (NotFoundException e) {
					// fall thru, it means there is no QR code in image
				}
			}
			
			if (result != null) {
				String name = loginService.openDoor(result.getText());
				if (name != null) { 
					textarea.append("\nACCESS GRANTED, Welcome " + name);
				} else {
					textarea.append("\nACCESS DENIED");
				}
			}

		} while (true);
		} catch(Throwable t) {
			t.printStackTrace();
		}
	}

	public Thread newThread(Runnable r) {
		Thread t = new Thread(r, "example-runner");
		t.setDaemon(true);
		return t;
	}

	public void setLoginService(ILoginService loginService) {
		this.loginService = loginService;
	}
}