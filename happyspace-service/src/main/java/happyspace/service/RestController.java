package happyspace.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import happyspace.persistence.User;
import happyspace.web.ILoginService;
import happyspace.web.LoginData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/")
public class RestController {
	@Autowired
	ILoginService loginService;
	
	@RequestMapping(value = "qr/{username}", method = RequestMethod.GET, headers = "Accept=image/jpeg, image/jpg, image/png, image/gif")
	public @ResponseBody
	byte[] getQrCodeImage(@PathVariable String username) {
		BufferedImage image = loginService.getQrCode(username);
		ByteArrayOutputStream bao = new ByteArrayOutputStream();
	    try {
			ImageIO.write(image, "jpg", bao);
		} catch (IOException e) {
			e.printStackTrace();
		}
	    return bao.toByteArray();
	}

	@RequestMapping(value= "login", method = RequestMethod.POST)
	public @ResponseBody
	User login(@RequestBody LoginData loginData) {
		return loginService.login(loginData);
	}
	
	@RequestMapping(value= "signup", method = RequestMethod.POST)
	public @ResponseBody
	User signUp(@RequestBody User newUser) {
		return loginService.signUp(newUser);
	}
}