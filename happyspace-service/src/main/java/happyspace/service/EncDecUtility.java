package happyspace.service;

import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.ImageIO;

import org.slf4j.LoggerFactory;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamMotionDetector;
import com.github.sarxos.webcam.ds.buildin.WebcamDefaultDevice;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;

import sun.misc.*;


public class EncDecUtility {
	private static final String ALGO = "AES";
	private static final byte[] keyValue = new byte[] { 'T', 'h', 'e', 'B',
			'e', 's', 't', 'S', 'e', 'c', 'r', 'e', 't', 'K', 'e', 'y' };

	@SuppressWarnings("restriction")
	public static String encrypt(String Data) throws Exception {
		Key key = generateKey();
		Cipher c = Cipher.getInstance(ALGO);
		c.init(Cipher.ENCRYPT_MODE, key);
		byte[] encVal = c.doFinal(Data.getBytes());
		String encryptedValue = new BASE64Encoder().encode(encVal);
		return encryptedValue;
	}

	@SuppressWarnings("restriction")
	public static String decrypt(String encryptedData) throws Exception {
		Key key = generateKey();
		Cipher c = Cipher.getInstance(ALGO);
		c.init(Cipher.DECRYPT_MODE, key);
		byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedData);
		byte[] decValue = c.doFinal(decordedValue);
		String decryptedValue = new String(decValue);
		return decryptedValue;
	}

	private static Key generateKey() throws Exception {
		Key key = new SecretKeySpec(keyValue, ALGO);
		return key;
	}
	
	public static void main(String args[]) {
		Webcam webcam = Webcam.getDefault();
		webcam.setViewSize(WebcamDefaultDevice.SIZE_QVGA);
		WebcamMotionDetector detector = new WebcamMotionDetector(webcam, 127, 1000);
		detector.setInterval(1000);
		detector.start();
		int index = 0;
		while (true) {
		    if (detector.isMotion()) {
		    	BufferedImage image = webcam.getImage();
		    	try {
					ImageIO.write(image, "PNG", new File("/home/ekrem/temp/test"+ index +".png"));
					index++;
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		    	Reader reader = new MultiFormatReader();
		    	LuminanceSource source = new BufferedImageLuminanceSource(image); 
				BinaryBitmap mapaBits = new BinaryBitmap(new HybridBinarizer(source)); 
				Result result;
				try {
					result = reader.decode(mapaBits);
					System.out.println("Motion detected! : " + result.getText());
				} catch (NotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ChecksumException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (FormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//		        System.out.println("Motion detected!");
		    }
		    try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

//		Webcam webcam = Webcam.getDefault();
//		BufferedImage image = webcam.getImage();
//		LuminanceSource source = new BufferedImageLuminanceSource(image); 
//		BinaryBitmap mapaBits = new BinaryBitmap(new HybridBinarizer(source)); 
//		Result result;
//		Reader reader = new MultiFormatReader();
//		try {
//			result = reader.decode(mapaBits);
//			System.out.println(result.getText());
//		} catch (NotFoundException e) {
//			e.printStackTrace();
//		} catch (ChecksumException e) {
//			e.printStackTrace();
//		} catch (FormatException e) {
//			e.printStackTrace();
//		}
		
//		Webcam webcam = Webcam.getDefault(); // non-default (e.g. USB) webcam can be used too
//		webcam.open();
//
//		Result result = null;
//		BufferedImage image = null;
//
//		if (webcam.isOpen()) {
//		    if ((image = webcam.getImage()) == null) {
//		        return;
//		    }
//
//		    LuminanceSource source = new BufferedImageLuminanceSource(image);
//		    BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
//		    try {
//		        result = new MultiFormatReader().decode(bitmap);
//		    } catch (NotFoundException e) {
//		        // fall thru, it means there is no QR code in image
//		    }
//		}
//
//		if (result != null) {
//		    System.out.println("QR code data is: " + result.getText());
//		}
		
	}
}
