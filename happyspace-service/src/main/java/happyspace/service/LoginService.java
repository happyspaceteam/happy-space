package happyspace.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.ImageIO;

import org.springframework.transaction.annotation.Transactional;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Writer;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import happyspace.dao.IUserDao;
import happyspace.persistence.User;
import happyspace.web.ILoginService;
import happyspace.web.LoginData;

public class LoginService implements ILoginService {
	private IUserDao userDao;
	private static final int width = 500;
	private static final int high = 500;

	public User login(LoginData loginData) {
		User user = userDao.getUserByUsername(loginData.getUserid());
		if (user != null && user.getPassword().equals(loginData.getPassword())) {
			return user;
		}
		return null;
	}

	@Transactional
	public User signUp(User newUser) {
		return userDao.persist(newUser);
	}

	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}

	public BufferedImage getQrCode(String username) {
		User user = userDao.getUserByUsername(username);
		String data = user.getName() + ";" + user.getPhoneNumber() + ";" + user.getEmail();
		try {
			data = EncDecUtility.encrypt(data);
			BitMatrix bm;
			Writer writer = new QRCodeWriter();
			bm = writer.encode(data, BarcodeFormat.QR_CODE, width, high);

			BufferedImage image = new BufferedImage(width, high, BufferedImage.TYPE_INT_RGB);
			for (int y = 0; y < width; y++) {
				for (int x = 0; x < high; x++) {
					int grayValue = (bm.get(x, y) ? 1 : 0) & 0xff;
					image.setRGB(x, y, (grayValue == 0 ? 0 : 0xFFFFFF));
				}
			}
			image = invertirColores(image);
			return image;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private BufferedImage invertirColores(BufferedImage image) {
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < high; y++) {
				int rgb = image.getRGB(x, y);
				if (rgb == -16777216) {
					image.setRGB(x, y, -1);
				} else {
					image.setRGB(x, y, -16777216);
				}
			}
		}
		return image;
	}

	public String openDoor(String text) {
		try {
			String data = EncDecUtility.decrypt(text);
			String elements[] = data.split(";");
			String name = elements[0];
			String phoneNumber = elements[1];
			String email = elements[2];
			User user = userDao.getUserByNamePhoneNumberAndEmail(name, phoneNumber, email);
			if (user != null) {
				return user.getName();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
